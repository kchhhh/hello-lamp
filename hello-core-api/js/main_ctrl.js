(function () {
  'use strict';

  angular.module('helloCoreApi').controller('MainCtrl', [
    '$location',
    '$routeParams',
    'c8yUser',
      MainCtrl
  ]);

  function MainCtrl(
    $location,
    $routeParams,
    c8yUser
  ) {
    console.log("mainCtrl");
    c8yUser.current().catch(function () {
      $location.path('/login');
    });
    console.log('mianCtrl deviceId',$routeParams.deviceId);

    if($routeParams.deviceId == undefined){
      if (!$routeParams.section) {
        $location.path('/');
      }

    }


    this.currentSection = $routeParams.section;
    this.deviceId = $routeParams.deviceId;
    this.sections = {
      Devices: 'devices',
      Alarms: 'alarms',
      Events: 'events'
    };
    this.filter = {};

    this.logout = function () {
      $location.path('/login');
    };
  }
})();
