(function () {
  'use strict';
  var app = angular.module('helloCoreApi', [
      'c8y.sdk',
      'c8y.ui',
      'ngRoute',
      'ui.bootstrap',
      'highcharts-ng'
    ]);
  app.config([
    '$routeProvider',
    configRoutes
  ]);
  app.config([
    'c8yCumulocityProvider',
    configCumulocity
  ]);

  function configRoutes(
    $routeProvider
  ) {
    $routeProvider
      .when('/login', {
        templateUrl: 'views/login.html',
        controller: 'LoginCtrl',
        controllerAs: 'login'
      })
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl',
        controllerAs: 'main'
      })
      .when('/:section', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl',
        controllerAs: 'main'
      })
      .when('/devices/:deviceId',{
          templateUrl: 'views/devices/detail.html',
          controller: 'MainCtrl',
          controllerAs: 'main'
      });
  }

  function configCumulocity(
    c8yCumulocityProvider
  ) {
    //c8yCumulocityProvider.setAppKey('core-application-key');
    c8yCumulocityProvider.setAppKey('demo');
    c8yCumulocityProvider.setBaseUrl('https://r45289.cumulocity.com/');
  }
    Highcharts.setOptions({
        timezoneOffset: 8*60
    });

})();
