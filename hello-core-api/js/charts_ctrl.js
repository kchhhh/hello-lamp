(function () {
    'use strict';


    angular.module('helloCoreApi').controller('ChartsCtrl', [
        '$scope', '$interval', '$routeParams', 'c8yDevices', 'c8yMeasurements', 'c8yBase', 'c8yDeviceControl', ChartsCtrl]);

    function ChartsCtrl($scope, $interval, $routeParams, c8yDevices, c8yMeasurements, c8yBase, c8yDeviceControl) {

        $scope.highchartsNG = {
            options: {
                chart: {
                    type: 'line',
                    zoomType: 'x',
                    height: 410
                }
            },
            title: {
                text: "Measurement vs. PWM"
            },
            xAxis: {
                type: 'datetime',
                dateTimeLabelFormats: {
                    // millisecond: '%H:%M:%S.%L',
                    // second: '%H:%M:%S',
                    minute: '%H:%M',
                    hour: '%H:%M',
                    day: '%m-%d',
                    // week: '%m-%d',
                    // month: '%Y-%m',
                    // year: '%Y'
                }
            },

            yAxis: [{ // Primary yAxis

                title: {
                    text: '',
                },
                opposite: true

            }, { // Secondary yAxis

                title: {
                    text: ''

                },

            }],

            series: [{
                type: 'line',
                name: '',
                data: [],
                yAxis: 0

            }, {
                type: 'line',
                name: '',
                data: [],
                yAxis: 1

            }],

            credits: {
                enabled: false
            },
            legend: {
                enabled: true
            },

            loading: false
        }

        if ($routeParams.deviceId != undefined) {
            //console.log('process device detail');
            var deviceId = $routeParams.deviceId;
            c8yDevices.detail(deviceId).then(function (res) {
                $scope.device = res.data;
                //console.log(JSON.stringify($scope.device));
            });

            var readMeasurement = function () {
                c8yMeasurements.list(
                    _.assign(c8yBase.todayFilter(), {
                        type: 'gdlk_smartLedMeasurement',
                        source: deviceId
                    })
                ).then(function (measurements) {
                    console.log('load data from server');
                    console.log('latest measurement %s', JSON.stringify(measurements));
                    $scope.measurements = measurements;
                    //console.log("measurement %s", JSON.stringify($scope.measurements));
                    $scope.powerMeasurement = [];
                    $scope.currentMeasurement = [];
                    $scope.voltageMeasurement = [];
                    for (var i = 0; i < $scope.measurements.length; i++) {
                        var item = $scope.measurements[i];
                        var timezoneOffset = 28800000;
                        $scope.powerMeasurement.push([Date.parse(item.time) + timezoneOffset, item.gdlk_smartLedMeasurement.Power.value]);
                        $scope.currentMeasurement.push([Date.parse(item.time) + timezoneOffset, item.gdlk_smartLedMeasurement.Current.value]);
                        $scope.voltageMeasurement.push([Date.parse(item.time) + timezoneOffset, item.gdlk_smartLedMeasurement.Voltage.value]);
                    }

                });

            };

            $scope.masterOps = {
                "deviceName": "gdlkSmartLedMasterCtrl",
                "status": "PENDING",
                "com_goldenpool_model_smartDevice": {
                    "name": "smartLedMasterCtrl",
                    "parameters": {
                        "ctrl5": {
                            "STDB": 20,
                            "PWM": 80,
                            "TM": {
                                "M": 0,
                                "H": 20
                            }
                        },
                        "ctrl4": {
                            "STDB": 20,
                            "PWM": 43,
                            "TM": {
                                "M": 30,
                                "H": 18
                            }
                        },
                        "ctrl3": {
                            "STDB": 20,
                            "PWM": 10,
                            "TM": {
                                "M": 0,
                                "H": 6
                            }
                        },
                        "ctrl2": {
                            "STDB": 20,
                            "PWM": 43,
                            "TM": {
                                "M": 0,
                                "H": 4
                            }
                        },
                        "ctrl1": {
                            "STDB": 20,
                            "PWM": 43,
                            "TM": {
                                "M": 0,
                                "H": 0
                            }
                        }
                    }
                },
                "description": "smart Led Master Control"
            };

            $scope.pwmSeries = [];

            var readPWMOperation = (function () {
                var c_date = new Date();
                var c_time = c_date.getHours(c_date) * 60 + c_date.getMinutes(c_date);
                var opsList = $scope.masterOps.com_goldenpool_model_smartDevice.parameters;

                //console.log("opsList %s", JSON.stringify(opsList));
                var timezoneOffset = 28800000;
                var c_pwm = [];

                for (var i = 1; i < 6; i++) {
                    var ctrllable = "ctrl" + i;

                    var tm = c_date.setHours(opsList[ctrllable].TM.H, opsList[ctrllable].TM.M, 0, 0);
                    c_pwm[i] = opsList[ctrllable].PWM;
                    if (i > 1) {
                        $scope.pwmSeries.push([tm + timezoneOffset - 10000, c_pwm[i - 1]]);
                        $scope.pwmSeries.push([tm + timezoneOffset, c_pwm[i]]);
                    } else {
                        $scope.pwmSeries.push([tm + timezoneOffset, c_pwm[i]]);
                    }
                }

                $scope.pwmSeries.push([new Date().setHours(23, 59, 59, 0) + timezoneOffset, c_pwm[5]]);

                console.log("$scope.pwmSeries %s", JSON.stringify($scope.pwmSeries));


            })();

            console.log("pwmSeries %s", JSON.stringify($scope.pwmSeries));
            var stopReadMeasurement = $interval(readMeasurement, 10000);


        }

        //show PWM

        var showPWM = (function () {
            $scope.highchartsNG.series[1].data = $scope.pwmSeries;
            $scope.highchartsNG.series[1].name = 'PWM Setting';
            $scope.highchartsNG.series[1].yAxis = 1;
            $scope.highchartsNG.yAxis.title = 'PWM';
            $scope.highchartsNG.yAxis[1].max = 100;
            $scope.highchartsNG.yAxis[1].min = 0;

        })();


        $scope.showMeasurement = function () {
            if ($scope.swapShow == 'Power') {
                $scope.swapShow = 'Current';
            }
            else if ($scope.swapShow == 'Current') {
                $scope.swapShow = 'Voltage';
            }
            else if ($scope.swapShowt == 'Voltage') {
                $scope.swapShow = 'Power';
            }
            else
                $scope.swapShow = 'Power';

            if ($scope.swapShow == 'Power') {
                this.highchartsNG.series[0].data = $scope.powerMeasurement;
                this.highchartsNG.series[0].name = 'Power Measurement'
                this.highchartsNG.series[0].yAxis = 0;
                this.highchartsNG.yAxis[0].max = 200;
                this.highchartsNG.yAxis[0].min = 0;


            } else if ($scope.swapShow == 'Current') {
                this.highchartsNG.series[0].data = $scope.currentMeasurement;
                this.highchartsNG.series[0].name = 'Current Measurement'
                this.highchartsNG.series[0].yAxis = 0;
                this.highchartsNG.yAxis[0].max = 10;
                this.highchartsNG.yAxis[0].min = 0;

            } else if ($scope.swapShow == 'Voltage') {
                this.highchartsNG.series[0].data = $scope.voltageMeasurement;
                this.highchartsNG.series[0].name = 'Voltage Measurement'
                this.highchartsNG.series[0].yAxis = 0;
                this.highchartsNG.yAxis[0].max = 200;
                this.highchartsNG.yAxis[0].min = 0;
            }
        };


    }

})();
